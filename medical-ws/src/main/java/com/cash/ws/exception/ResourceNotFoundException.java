package com.cash.ws.exception;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	
	
	public Map<String,String> ResourceNotFoundException() {
		Map<String,String> result= new LinkedHashMap<String, String>();
		result.put("codigo", "400");
		result.put("mensaje", "El id de usuario no existe");
		return result;
	}

	
}
