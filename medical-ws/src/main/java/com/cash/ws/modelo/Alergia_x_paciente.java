package com.cash.ws.modelo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "alergias_paciente")
public class Alergia_x_paciente {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Integer alergia_x_paciente_id;
	
	private Integer alergia_id;
	
	private Integer paciente_id;
	private Date fecha_alta;
	private String medicamento;
	
	public Integer getAlergia_x_paciente_id() {
		return alergia_x_paciente_id;
	}
	public void setAlergia_x_paciente_id(Integer alergia_x_paciente_id) {
		this.alergia_x_paciente_id = alergia_x_paciente_id;
	}
	public Integer getAlergia_id() {
		return alergia_id;
	}
	public void setAlergia_id(Integer alergia_id) {
		this.alergia_id = alergia_id;
	}
	public Integer getPaciente_id() {
		return paciente_id;
	}
	public void setPaciente_id(Integer paciente_id) {
		this.paciente_id = paciente_id;
	}
	public Date getFecha_alta() {
		return fecha_alta;
	}
	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}
	public String getMedicamento() {
		return medicamento;
	}
	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}
	
	

}
