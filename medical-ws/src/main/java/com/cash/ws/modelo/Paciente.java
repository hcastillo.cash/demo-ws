package com.cash.ws.modelo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity // This tells Hibernate to make a table out of this class
@Table(name = "pacientes")
public class Paciente {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer paciente_id;
	
	private Integer paciente_no_expediente;
	private String  paciente_tipo_sangre;
	private Date paciente_fecha_ultima_consulta;
	
	public Integer getPaciente_id() {
		return paciente_id;
	}



	public void setPaciente_id(Integer paciente_id) {
		this.paciente_id = paciente_id;
	}



	public Integer getPaciente_no_expediente() {
		return paciente_no_expediente;
	}



	public void setPaciente_no_expediente(Integer paciente_no_expediente) {
		this.paciente_no_expediente = paciente_no_expediente;
	}



	public String getPaciente_tipo_sangre() {
		return paciente_tipo_sangre;
	}



	public void setPaciente_tipo_sangre(String paciente_tipo_sangre) {
		this.paciente_tipo_sangre = paciente_tipo_sangre;
	}



	public Date getPaciente_fecha_ultima_consulta() {
		return paciente_fecha_ultima_consulta;
	}



	public void setPaciente_fecha_ultima_consulta(Date paciente_fecha_ultima_consulta) {
		this.paciente_fecha_ultima_consulta = paciente_fecha_ultima_consulta;
	}
	

}
