package com.cash.ws.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity // This tells Hibernate to make a table out of this class
@Table(name = "cat_alergias")
public class Alergia {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer alergia_id;
	
	private String alergia_nombre;

	public Integer getAlergia_id() {
		return alergia_id;
	}

	public void setAlergia_id(Integer alergia_id) {
		this.alergia_id = alergia_id;
	}

	public String getAlergia_nombre() {
		return alergia_nombre;
	}

	public void setAlergia_nombre(String alergia_nombre) {
		this.alergia_nombre = alergia_nombre;
	}	 
	
	
}
