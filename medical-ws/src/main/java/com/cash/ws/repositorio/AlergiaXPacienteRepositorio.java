package com.cash.ws.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.cash.ws.modelo.Alergia_x_paciente;

public interface AlergiaXPacienteRepositorio  extends CrudRepository<Alergia_x_paciente, Integer>{

}
