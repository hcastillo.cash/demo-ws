package com.cash.ws.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.cash.ws.modelo.Alergia;


public interface AlergiaRepositorio extends CrudRepository<Alergia, Integer> {

}
