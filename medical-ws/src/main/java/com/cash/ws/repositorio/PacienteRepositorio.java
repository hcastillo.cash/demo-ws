package com.cash.ws.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.cash.ws.modelo.Paciente;

public interface PacienteRepositorio extends CrudRepository<Paciente, Integer> {
	
}
